let fivePowerOf3 = 5**3;
console.log(fivePowerOf3); //125


/*
let fivePowerOf2 = Math.pow(5,2);
let fivePowerOf4 = Math.pow(4,5);
let lettwoPowerOf2 = Math.pow(2,2);
*/

let fivePowerOf2 = 5**2;
let fivePowerOf4 = 5**4;
let twoPowerOf2 = 2**2;

console.log(fivePowerOf2); 
console.log(fivePowerOf4);
console.log(twoPowerOf2);




/*
	Mini-Activity

	Debug the following code that uses ES6 Exponent Operator:

*/

function squareRootChecker(number){
	return number**.5;
}

let squareRootOf4 = squareRootChecker(4);
console.log(squareRootOf4);



/*
	Template Literals


	An Es6 update to creating strings. Represented by backticks(``).	
*/

let sampleString1 = `Charlie`;
let sampleString2 = `Joy`;
console.log(sampleString1);
console.log(sampleString2);
// Charlie and Joy into 1 string? "Charlie Joy"
// let combinedString = `Charlie` + ` ` + `Joy`;
// console.log(combinedString)

let combinedString = `${sampleString1} ${sampleString2}`;
console.log(combinedString)
// We can create a single string to combine our variables with the use of template literals(``) and by embedding JS exppressions and variable in the string using ${} or placeholders.

console.log(combinedString);

/*Template Literals with JS Expressions*/
let num1 = 15;
let num2 = 3;
let sum = num1 + num2;
//What if we want to say a sentence in the console like this:
//"The result of 15 plus 3 is 18"
// let sentence = "The result of " + num1 + " plus " + num2 + " is " + sum;
let sentence = `The result of ${num1} plus ${num2} is ${num1+num2}`
console.log(sentence);



/* Array Destructuring */
/* When we destructure an array, we save array items in variables */
let array = ["Kobe", "Lebron", "Jordan"];

/*
let goat1 = array[0];
console.log(goat1);
let goat2 = array[1];
let goat3 = array[2];
console.log(goat2,goat3);
*/
let [goat1,goat2,goat3] = array;
console.log(goat1);
console.log(goat2);
console.log(goat3);


let array2 = ["Curry", "Lillard", "Paul", "Irving"];

/*
	Mini-Activity

	Save the items in the array into the following variables:

	pg1 = Curry,
	pg2 = Lillard,
	pg3 = Paul,
	pg4 = Irving

	Log the values of each variable in the console.
*/

let [pg1,pg2,pg3,pg4] = array2
console.log(array2)





// We can save the values of an object's properties into variables.
// By Object Destructuring:

/*
	Array Destructuring, order was important and the name of the variable we save our items in is not important.

	Object destructuring, order is not important however the name of the variables should match the properties of the object.
*/


let pokemon = {
	name: "Blastoise",
	level: 10,
	health: 100
}

let {health,name,level, trainer} = pokemon;
console.log(health);
console.log(name);
console.log(level);
console.log(trainer);

let person = {
	name: "Paul Phoenix",
	age: 31,
	birthday: "January 12, 1991"
}

/* Create a function which is able to receive an object as an argument. It should be able to destructure the object in the funtion and display the following messages in the console:

"Hi! My name is <nameOfPerson."
"I am <ageOfPerson> years old."
"My birthday is on <birthdayOfPerson>."

*/


let person1 = {
	name: "Aaron",
	age: 30,
	birthday: "May 21, 1991"
}


function greet(name,age,birthday){

	console.log(`Hi! My name is ${name}.`);
	console.log(`I am ${age} years old.`);
	console.log(`My birthday is on ${birthday}.`)
}


console.log(greet)


let actors = ["Tom Hanks", "Leonardo DiCarpio", "Anthony Hopkins", "Ryan Reynolds"];

let director = {
	name: "Alfred Hitchcock",
	birthday: "August 13, 1889",
	isActive: false,
	movies: ["The Birds", "Psycho", "North by Northwest"]
};


let [actor1, actor2, actor3] = actors;

function displayDirector(person){
	let {name,birthday,movies} = person;

	console.log(`The Director's name is ${name}`);
	console.log(`He was born on ${birthday}`);
	console.log(`His Movies include:`);
	console.log(`${movies}`);
}

console.log(actor1);
console.log(actor2);
console.log(actor3);

displayDirector(director);


// ARROW FUNCTION
const hello = () => {
	console.log(`Hello from Arrow!`)
}

hello();

function greet(personParams){
	console.log(`Hi, ${person.Params.name}!`);
}



// Mini-Activity - change "function greet" into an Arrow-Function

const greet1 = () => {
	console.log(`Hi!, ${name}!`)
}

greet1()




// ANONYMOUS FUNCTIONS in array methods

/*
	It allows us to iterate/loop over all items in an array.
	An anonymous funtion is added so we can perform tasks per item in the array.
	This anonymous function receives the current item being iterated/looped.
*/

// Note: traditional functions cannot work without the curly braces.
// Note: Arrow Functions can BUT it has to be a one-liner
actors.forEach(actor=>console.log(actor));

// .map() is similar to forEach wherein we can iterate over all items in our array.
//  the difference is we can return items from a map and create a new array out of it.
// Arrow functions do not need the return keyword to return a value. This is called implicit return.

let numberArr = [1,2,3,4,5];

/*
let multipliedBy5 = numberArr.map(function(number){
	return number*5 
})

*/

// Mini-Activity: recreate/update the anonymous function in the map as an arrow function.

let multipliedBy5 = numberArr.map(number=>number*5)
console.log(multipliedBy5)




/*
	Implicit Return for Arrow Functions
*/


/*
function addNum(num1,num2){
	return(num1+num2);
};

let sum1 = addNum(5,10);
console.log(sum1);
*/


const subtractNum = (num1,num2) => num1-num2;
let difference1 = subtractNum(45,15);
console.log(difference1);
// Even without a return keyword, arrow functions can return a value so long as its code block is not wrapped in {}.


// Mini-Activity: translate the traditional function addNum() into an arrow function with implicit return.


// Updated addNum()

const addNum = (num1,num2) => num1+num2;
let sumExample = addNum(50,10);
console.log(sumExample)



/* THIS keyword in a method */

let protagonist = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
	greet: function(){
		console.log(this); //this refers to the object where the method is in.
		console.log(`Hi! I am ${this.name}`);
	},
	introduceJob: () => {
		console.log(this) //this keyword in an arrow function refers to the global window object/whole page and not to this parent's object.
		console.log(`I work as a ${this.occuptaion}`)
	}
}
protagonist.greet(); //this refers to parent object.
protagonist.introduceJob(); //this.occupation results to undefined because this in an arrow function refers to the global window object.


